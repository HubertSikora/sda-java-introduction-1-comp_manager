package zajecia7.inheritance;

public class Person {
    //pola
    protected String name;
    private String surname;
    private int age;

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Person() { // Pusty konstruktor ponieważ wtedy podkreśla extends.
    } // alt + insert >> construktor >> Select nono

    //konstruktor
    public Person(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public String describe() {
        return this.name + " " + this.getSurname() + " ";
    }


}
