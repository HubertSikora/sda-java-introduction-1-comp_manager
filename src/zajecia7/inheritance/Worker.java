package zajecia7.inheritance;

public class Worker extends Person {
    private double pensja;
    private String stanowisko;

    public Worker() {
    }

    public double getPensja() {
        return pensja;
    }

    public String getStanowisko() {
        return stanowisko;
    }

    public Worker(double pensja, String stanowisko) {
        this.pensja = pensja;
        this.stanowisko = stanowisko;

    }

    public String toString() {
        return String.format("Pracownik: %d %s", getPensja(), getStanowisko());
    }
}
