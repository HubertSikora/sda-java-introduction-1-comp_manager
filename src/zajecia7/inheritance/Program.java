package zajecia7.inheritance;

public class Program {
    public static void main(String[] args) {
        Person person = new Person("Hubert", "Programista", 20);
        System.out.println(person.describe());

        Student student = new Student("Jan", "Nowak", 50); //CTRL+P wyświetla podpowiedzi
        System.out.println(student.describe()); // opisuje osobę czyli zwraca imię i nazwisko
        System.out.println(student.getIndex());

        Student studentInformatyki = new Student("Kasia", "Jabłońska", 50, 10795, "Politechnika Krakowska");
        System.out.println(studentInformatyki.describe());


        System.out.println(studentInformatyki);
        Worker pracujacy = new Worker(2000,"Kasjer");
        System.out.println(pracujacy); // wywoluje pracujacy.toString tyle ze toString jest nie jawne wiec nie trzeba pisac.

    }
}
