package zajecia7.inheritance;

//rozszezamy studenta o osobe czyli dziedziczymy

public class Student extends Person {


    //pola
    int index;
    String university;

    public Student(String name, String surname, int age) {
        super(name, surname, age); // słowo super oznacza wywoływanie konstr z nad klasy
    }

    //kolejny konstr
    public Student (String name, String surname, int age, int index, String university){
        super (name, surname,age);
        this.index = index;
        this.university = university;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
    public String getUniversity(){ // getter nie ma argumentu, zwraca bez argumentu czyli nie wpisywać nic w nawiasy
        return university;
    }

@Override //adnotacja. Jeśli się pomylę to komplator zwroci błąd. Przeciazenie a nie przeładowanie.
//moim zamiarem bylo nadpisanie tej metedy. Overreading zamiast overloading.
//widać, że to jest z nad klasy.

    public String describe() {
        return String.format("%s %s %s %d" , getName(),getSurname(), getUniversity(), getIndex());
}
    //teraz przeladujemy. CTRL+O toString >> i wklejamy co ma pokazać ten obiekt wpisując poprostu sout(student)


    @Override
    public String toString() {
        return String.format("%s %s %s %d" , getName(),getSurname(), getUniversity(), getIndex());
    }
}
