package zajecia7;

import java.util.Random;

public class Matrix {
    //nie robimy dla "i" getterow i setterow aby jej nie wystawiac na zewnatrz
    private int[][] matrix;
    private int x;
    private int y;

    public Matrix() {
    }

    ; //tu konstr bez parametrowy

    public Matrix(int x, int y) { //tu jest konstruktor
        this.x = x;
        this.y = y;
        matrix = new int[x][y];
    }

    public int getX() {
        return this.x; //w tym przypadk this jest nadmiarowy.
    }

    public int getY() {
        return this.y;
    }

    /**
     * Adds second Matrix given as a paramiter
     *
     * @param second
     * @return
     */

    public Matrix addMatrix(Matrix second) { //macierz do dodania
        //najpierw sprawdz czy wymiary drugiej sie zgadzaja
        if (this.x != second.x || this.y != second.y) {
            throw new ArithmeticException("Wymiar macierzy");
        }
        //przejscie do obliczen

        Matrix result = new Matrix(x, y); // macierz wynikowa
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                result.matrix[i][j] = this.matrix[i][j] + second.matrix[i][j];
            }
        }
        return result;
    }

    public void print() {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void fillWithRandomValues() { //wypełnianie matrixa
        Random random = new Random();
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                matrix[i][j] = random.nextInt(55);
            }
        }
    }
}

