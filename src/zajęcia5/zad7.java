package zajęcia5;

import java.util.Random;
import java.util.Scanner;

public class zad7 {
    // deklaracja. TEraz mozemy miec w kazdej tablicy ta sama wartosc. To nie musi sie nazywac ARRAY_SIZE.
    //inaczje to byla by efektywna finalnosc jesli sobie obiecamy ze nic nie zimienimy ale mozemy. A w finalnej wartosci nie mozna nic zmienic bo wywali blad.
    public static final int ARRAY_SIZE = 50;
    //okresla gorne ograniczenie generatora liczb pseudolosowych;
    public static final int RANDOM_UPPER_BOUND = 50;

    public static void main(String[] args) {

        //System.out.println("");
       // Scanner sc = new Scanner(System.in);
        Random rand = new Random();
        int[] arr = new int[ARRAY_SIZE];
        int[] arr2 = new int[ARRAY_SIZE];
        // losowanie wartosci
        for (int i = 0; i < ARRAY_SIZE; i++) {
            arr[i] = rand.nextInt(RANDOM_UPPER_BOUND) + 1;
            arr2[i] = rand.nextInt(RANDOM_UPPER_BOUND) + 1;
        }
        //obliczenie wartosci
        int suma = 0;
        for (int i = 0; i < ARRAY_SIZE; i++) {
            suma += arr[i] * arr2[i];
            System.out.println("Iloczyn skalany wektorow "+suma);
        }System.out.println("ostatni"+suma);
    }
}
