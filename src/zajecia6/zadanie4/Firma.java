package zajecia6.zadanie4;

public class Firma {
    public static final int DEFAULT_SIZE = 50;
    private String name;
    private Firma[] firma;
    private int companSize;

    public Firma(String empName) {
        this.name = name;
        this.firma = new Firma[DEFAULT_SIZE];
        companSize = 0; // przy tworzeniu kazadej nowej firmy wstaw zero pracownikow
    }

    public Firma(String name, int initialSize) {
        this.name = name;
        this.firma = new Firma[initialSize];

    }

    public boolean addFirma(Firma firemka) {
        if (companSize < firma.length) {
            firma[companSize++] = firemka;
            return true;
        }
        return false;
    }

}
