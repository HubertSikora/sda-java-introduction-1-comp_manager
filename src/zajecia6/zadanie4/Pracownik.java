package zajecia6.zadanie4;

public class Pracownik {
    private String name;
    private String email;
    private double pensja;
    private int wiek;
    private int id;

    private Pracownik (String name, String email, double pensja, int id){
        this.name = name;
        this.email= email;
        this.pensja= pensja;
        this.id= id;
    }
    public Pracownik(String name, String email, double pensja, int wiek, int id){
        this(name, email, pensja, id);
        this.wiek= wiek;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getPensja() {
        return pensja;
    }

    public void setPensja(double pensja) {
        this.pensja = pensja;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    //metoda
    public String getDescription(){
        return String.format("Name:%s, adres email %s , salary: %s wiek %s , id %s", name, email, pensja, wiek,id);
    }
}
