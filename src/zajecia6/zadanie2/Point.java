package zajecia6.zadanie2;

public class Point {
    private int x;
    private int y;

//    public Point(x,y) {
//        this.x = x;
//        this.y = y;
//    }
//
//    public Point() {
//        System.out.println("bezparametrowy");
//    }
    // gettery i settery

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;

    }

    public void setY(int y) {
        this.y = y;
    }

    //metoida obliczajaca odlegosc od srodka
    public double distanceFromOrgin() {
        return Math.sqrt((x * x) + (y * y));
    }
}
