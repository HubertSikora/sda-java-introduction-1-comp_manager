package zajecia6;


/**
 * Klasa do reprezentacji prostokata
 */
public class Rectangle {
    // pola do opisywania wlasciwosci prostokata
    private double a;
    private double b;

    //getter i setter dla pola a
    public double slon() { //za slon getA
        return a;
    }

    public void setA(int a) {
        if (a <= 0) {
            throw new IllegalArgumentException("Dlugosc mniejsza od 0");
        }
        this.a = a;
    }

    //getter i setter dla pola b
    public double getB() {
        return b;
    }

    public void setB(double b) {
        if (b <= 0) {
            throw new IllegalArgumentException("Dlugosc mniejsza od 0");
        }
        this.b = b;
    }

    // metoda obliczajaca pole
    public double field() {
        return a * b;
    }

    //metoda obliczajaca obwod
    public double circumference() {
        return 2 * a + 2 * b;
    }
}
