package companyManager;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType (XmlAccessType.PROPERTY)
public class Employee {
    private int id;
    private String firstName;
    private String surname;
    private String email;
    private double salary;
    private int age;

    public Employee(String firstName, String surname) {
        this.firstName = firstName;
        this.surname = surname;
    }

    public Employee(String firstName, String surname, double salary) {
        this(firstName, surname);
        this.salary = salary;
    }

    public Employee(String firstName, String surname, String email, double salary) {
        this(firstName, surname, salary);
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDescription() {
        return String.format("Name: %s, Surname: %s, Salary: %f", firstName, surname, salary);
    }

    @Override
    public String toString() {
        return String.format("%d %s %s %s %.2f %d",
                id,
                firstName,
                surname,
                email,
                salary,
                age
        );
    }
}
