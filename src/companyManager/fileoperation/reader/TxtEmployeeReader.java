package companyManager.fileoperation.reader;

import companyManager.Employee;
import companyManager.fileoperation.util.ParseUtil;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class TxtEmployeeReader extends AbstractEmployeeReader {
    public TxtEmployeeReader(String pathToFile) {
        super(pathToFile);
    }

    @Override
    public Employee[] readEmployees() {
        Employee[] employees = null;
        int i = 0;
        // Try with resources
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(pathToFile))) {
            //odczyt ilosci pracownikow (zakladamy, ze liczba jest w pierwszej linii)
            String companySize = bufferedReader.readLine();
            //utworz tablice dla pracownikow z pliku
            //odczytana ilosc bedzie typu String
            //wiec musimy ja "zparsowac", czyli przekonwertowac typ String na int
            // parsowanie - konwersja z typu String
            employees = new Employee[Integer.parseInt(companySize)];
            //Odczyt wszystkich linii z pliku
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                //pobierz 1 linie i podziel po separatorze (u nas separatorem jest spacja)
                String[] split = line.split(" ");
                //wg struktury: id imie nazwisko email pensja wiek
                Employee emp = new Employee(split[1], split[2]);
                //ustawiamy odpowiednie pola wg zalozonego schematu
                emp.setId(Integer.parseInt(split[0]));
                emp.setEmail(split[3]);
                emp.setSalary(ParseUtil.parseDouble(split[4]));
                emp.setAge(Integer.parseInt(split[5]));
                //wpisujemy utworzonego pracownika do tablicy pracownikow
                employees[i++] = emp;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //zwracamy tablice pracownikow
        return employees;
    }
}
