package companyManager.fileoperation.writer;

import companyManager.Company;
import companyManager.Employee;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class XmlaEmployeeWriter extends AbstractEmployeeWriter {
    public XmlaEmployeeWriter(String pathToFile) {
        super(pathToFile);
    }

    @Override
    public Employee[] writeEmployees(Employee[] employees) {
        Company companyToSave = new Company();

        try {
            JAXBContext context = JAXBContext.newInstance(Company.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(companyToSave, new File(pathToFile));
            Object obj = unmarshaller = context.createUnmarshaller();
            Object unmarshal = unmarshaller.unmarshal (new File(pathToFile));
            Company result = (Company) obj;
            return result.getEmployees();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return employees;
    }
}
