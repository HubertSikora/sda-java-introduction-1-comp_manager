package companyManager.fileoperation.writer;

import companyManager.Employee;

public interface EmployeeWriter {
    Employee[] writeEmployees(Employee[] employees);
}
