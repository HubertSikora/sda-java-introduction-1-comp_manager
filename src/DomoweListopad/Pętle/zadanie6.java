package DomoweListopad.Pętle;

public class zadanie6 {
    public static void main(String[] args) {
//for
        for (int i = 65; i <= 90; i++) {
            System.out.print(" " + (char) i);
        }
        System.out.println();
        for (int i = 90; i >= 65; i--) {
            System.out.print(" " + (char) i);
        }
        System.out.println();
        // while
        int i = 65;
        while (i <= 90) {
            System.out.println("while: " + (char) i);
            i++;
        }
        int licznik = 65;
        do {
            System.out.println("petla DO "+ (char) licznik);
            licznik++;
        }
        while (licznik <=90);

    }
}
