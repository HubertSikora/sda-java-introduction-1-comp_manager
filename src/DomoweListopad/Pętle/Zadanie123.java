package DomoweListopad.Pętle;

public class Zadanie123 {
    public static void main(String[] args) {
        //wypisuje liczby calkowite od 1 do 100 for a pozniej while;
        //wyswietl wszystkie liczby podzielne przez 7 od 0 do 50;
        //dodaj te liczby

        int j = 0;
        for (int i = 0; i <= 50; i++) {
            double modulo = i % 7;
            if (modulo == 0) {

                j += i;
                System.out.println(i);

            }

        }
        System.out.println("suma z pętlą for: " + j);

//         z wykorzsytaniem while:

        int i = 0;
        double modulo = 0;
        int jj = 0;
        while (i <= 50) {
            modulo = i % 7;
            if (modulo == 0) {
                System.out.println("Pętla wypisana whilem: " + i);
                jj += i;
            }


            i++;

        }
        System.out.println("suma wszttstkich podzielnych przez 7 z pętlą while " + jj);
    }
}
