package DomoweListopad.Pętle;

public class zad5PorownanieTrzechPetli {
    public static void main(String[] args) {
        /**
         * Program który wypisuje liczby parzyste
         */

        // opcja swobodna
        for (int i = 0; i <= 10; i++) {
            double modulo = i % 2;
            if (modulo == 0) {
                System.out.println("pentla for" + i);

            }

        }
        //wersja z while
        int licznik = 0;
        double modulo = 0;
        while (licznik <= 10) {
            if (modulo == 0) {
                System.out.println("pentla while" + licznik);
            }
            licznik++;
            modulo = licznik % 2;

        }
        //z uzyciem DO WHILE
        int licznikk = 0;
        double m = 0;
        do {
            if (m == 0)
                System.out.println("Pentla do while: " + licznikk);
            licznikk++;
            m = licznikk %2;
        }
        while (licznikk <= 100);

    }
}
