package DomoweListopad.Tablice.KółkoIKrzyzyk;

import java.util.Scanner;

//dane
public class KolkoKrzyzyk {
    static char[][] table = {{' ', ' ', ' '},
            {' ', ' ', ' '},
            {' ', ' ', ' '}};
    static Scanner sc = new Scanner(System.in);
//    static String Player1Name;
   static char LiteraPlayer1Name;
//    static String Player2Name;
    static char LiteraPlayer2Name;
    static char wygrany;

    //metody
//wyswietla plansze
    public static void Plansza() {
        System.out.println();
        System.out.println("    1 2 3");
        System.out.println("a   " + table[0][0] + "|" + table[0][1] + "|" + table[0][2]);
        System.out.println("b   " + table[1][0] + "|" + table[1][1] + "|" + table[1][2]);
        System.out.println("c   " + table[2][0] + "|" + table[2][1] + "|" + table[2][2]);
    }
// kto wygrał?

    private static void checkResult() {
        if (table[0][0] == table[1][0] && table[1][0] == table[2][0] && table[2][0] != ' ') {
            wygrany = table[0][0];
        } else if
                (table[0][1] == table[1][1] && table[1][1] == table[2][1] && table[2][1] != ' ') {
            wygrany = table[0][1];
        } else if
                (table[0][2] == table[1][2] && table[1][2] == table[2][2] && table[2][2] != ' ') {
            wygrany = table[0][2];
        } else if
                (table[0][0] == table[0][1] && table[0][1] == table[0][2] && table[0][2] != ' ') {
            wygrany = table[0][1];
        } else if
                (table[1][0] == table[1][1] && table[1][1] == table[1][2] && table[1][2] != ' ') {
            wygrany = table[1][0];
        } else if
                (table[2][0] == table[2][1] && table[2][1] == table[2][2] && table[2][2] != ' ') {
            wygrany = table[2][0];
        } else if
                (table[0][0] == table[1][1] && table[1][1] == table[2][2] && table[2][2] != ' ') {
            wygrany = table[0][0];
        } else if
                (table[0][2] == table[1][1] && table[1][1] == table[2][0] && table[2][0] != ' ') {
            wygrany = table[0][2];
        } else if (table[0][0] != ' ' && table[1][0] != ' ' && table[2][0] != ' ' &&
                table[0][1] != ' ' && table[1][1] != ' ' && table[2][1] != ' ' &&
                table[0][2] != ' ' && table[1][2] != ' ' && table[2][2] != ' ') {
           wygrany = '0';
        }}


        //ruch gracza 1

    public static void RuchPlayer1() {
        boolean correct = false;
        System.out.println("Podaj wspolrzedne bez spacji (np: a1): ");
        while (!correct) {
            String coord = sc.next();
            if (coord.length() != 2) {
                System.out.println("Nieprawidlowe wspolrzedne");
            } else if (coord.charAt(0) != 'a' &&
                    coord.charAt(0) != 'b' &&
                    coord.charAt(0) != 'c') {
                System.out.println("Nieprawidlowy poczatek wspolrzednych (moze byc a, b lub c)");
            } else if (coord.charAt(1) != '1' &&
                    coord.charAt(1) != '2' &&
                    coord.charAt(1) != '3') {
                System.out.println("Nieprawidlowy koniec wspolrzednych (moze byc 1, 2 lub 3)");
            } else {
                int x;
                int y;
                if (coord.charAt(0) == 'a') {
                    x = 0;
                } else if (coord.charAt(0) == 'b') {
                    x = 1;
                } else {
                    x = 2;
                }
                if (coord.charAt(1) == '1') {
                    y = 0;
                } else if (coord.charAt(1) == '2') {
                    y = 1;
                } else {
                    y = 2;
                }
                if (table[x][y] != ' ') {
                    System.out.println("To pole jest już zajęte");
                } else {
                    table[x][y] = LiteraPlayer1Name;
                    correct = true;
                }
            }
        }
    }    //ruch gracza 2

    public static void RuchPlayer2() {
        boolean correct = false;
        System.out.println("Podaj wspolrzedne bez spacji (np: a1): ");
        while (!correct) {
            String coord = sc.next();
            if (coord.length() != 2) {
                System.out.println("Nieprawidlowe wspolrzedne");
            } else if (coord.charAt(0) != 'a' &&
                    coord.charAt(0) != 'b' &&
                    coord.charAt(0) != 'c') {
                System.out.println("Nieprawidlowy poczatek wspolrzednych (moze byc a, b lub c)");
            } else if (coord.charAt(1) != '1' &&
                    coord.charAt(1) != '2' &&
                    coord.charAt(1) != '3') {
                System.out.println("Nieprawidlowy koniec wspolrzednych (moze byc 1, 2 lub 3)");
            } else {
                int x;
                int y;
                if (coord.charAt(0) == 'a') {
                    x = 0;
                } else if (coord.charAt(0) == 'b') {
                    x = 1;
                } else {
                    x = 2;
                }
                if (coord.charAt(1) == '1') {
                    y = 0;
                } else if (coord.charAt(1) == '2') {
                    y = 1;
                } else {
                    y = 2;
                }
                if (table[x][y] != ' ') {
                    System.out.println("To pole jest już zajęte");
                } else {
                    table[x][y] = LiteraPlayer2Name;
                    correct = true;
                }
            }
        }
    }


    public static void main(String[] args) {
        System.out.println("Przyjedźmy do menu");
        Menu.Menu();
        System.out.println();
        Plansza();
        for (int i = 0; i < 5; i++) {
            RuchPlayer1();
            Plansza();
            RuchPlayer2();
            Plansza();
            checkResult();
            System.out.println("Gratulujemy! wygrany to:  " + wygrany);

        }
    }
}
