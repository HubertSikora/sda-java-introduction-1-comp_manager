package zajecia3;

import java.util.Random;


public class FindMaxInArray {
    public static void main(String[] args) {
        Random losowanie = new Random();
        int[] tablica = new int[10];

        // wpisywanie losowych wartosci to tablicy
        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = losowanie.nextInt(150); //bound okresla granicę ,zakres losowania
        }

        // wypisz wartosci
        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i] + " ");
        }

        // szukanie max
        // zakladamy wstepnie, ze najwieksza wartosc jest w tablicy o indeksie 0
        //jezeli spotkamy wieksza od aktualnego maximum
        //to podmieniamy
        int max = tablica[0];
        //ponizej nie ma sensu zaczyznac od zera
        for (int i = 1; i < tablica.length; i++) {
            if (tablica[i] > max) {
                max = tablica[i];
                System.out.println('\n'+"aktualny max:" +max);
            }
        }

        System.out.println("\nNajwieksza wartosc to: " + max);
        System.out.println("Jakaś wylosowana liczba z pewnego zakresu " +losowanie.nextInt(150));
    }
}

