package zajecia3;

import java.util.Scanner;

public class PetleWhile {
    public static void main(String[] args) {
        System.out.println("podaj zakres zliczb");
        Scanner sc = new Scanner(System.in);
        //zapytaj u zakres loczb
        int a = sc.nextInt();
        int b = sc.nextInt();

        //wypisac parzyste z teo zakresu
        // a= 10 b=50
        while (a < b) {
            if (a % 2 == 0){
                System.out.println(a);}
            a++;
        }


    }

    // zadanie 10 . Factorial czyli silnia po angielsku
    // shift + F6 czyli refactoryzacja rename nazw
    public static class Silnia {
        public static double factorial(double n) {
            int wynik = 1;
            for (int i = 1; i <= n; i++) {
                wynik = i * wynik;
            }
            return wynik;
        }


        //    public static void main(String[] args) {
    //        int suma=0;
    //        int n=10;
    //        for (int i=1; i <= n; i++){
    //
    //            suma=i*suma;
    //            System.out.println(suma);
    //
    //        }
    //
    //}
        public static void main(String[] args) {

            System.out.println(factorial(5));
        }
    }
}
