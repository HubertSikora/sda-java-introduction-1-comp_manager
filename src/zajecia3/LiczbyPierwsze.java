package zajecia3;

import java.util.Scanner;

public class LiczbyPierwsze {
    public static boolean isPrime(int number) {
        boolean result = true;
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                result = false;
                break; // zakladamy ze jesli znajdzie cos juz
                // w pierwszym obiegu to ma skonczyc i wyskoczy do pierwszej instrukcji po za pętlą

            }
        }
        return result;
    }
//  Debugr:F7 wchchodzimy do metody albo ustawiamy w niej brakpoint

    public static void main(String[] args) {
        System.out.println("Podaj liczbę pierwsza: ");
        Scanner sc = new Scanner(System.in);
        int liczba = sc.nextInt();
        boolean wynik = isPrime(liczba);
        System.out.println(wynik);


    }
}
