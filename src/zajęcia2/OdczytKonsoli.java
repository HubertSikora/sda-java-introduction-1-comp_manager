package zajęcia2;

import java.util.Scanner;

/**
 * Created by Hubert on 2017-10-22.
 */
public class OdczytKonsoli {
    public static void main (String [ ] args){
        String imie;
        String nazwisko;
        int wiek;

        System.out.println("Jak masz na imię?"); // CTRL+P w nawiasie podpowiada co mozna wpisać
        Scanner odczyt = new Scanner(System.in) ; //Utworzenie obiektu typu skaner o nazwie odczyt
        imie = odczyt.nextLine(); //do zmiennej imie wstaw linię z konsoli. Nie Hardcodujemy już.
        //CTRL+spacja przywróci nam listę rozwijalna z podwiedzidzia.
        // ALT+1 - pasek projektu pokaż/ukryj
        System.out.println("Podaj naziwsko");
        nazwisko = odczyt.nextLine();
        System.out.println("Witaj "+imie+" "+ nazwisko +"!!!");
        System.out.println("Ile masz lat?");
        wiek = odczyt.nextInt();
        System.out.println("Nazwyasz się " + imie +" " +nazwisko+ " i masz  " + wiek+ " lat.");

    }
}