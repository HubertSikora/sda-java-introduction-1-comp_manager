package zajęcia2.zajęcia2.konwenter;

import java.util.Scanner;

/**
 * Created by Hubert on 2017-10-22.
 * <p>
 * Program służacy do konwersji wartości temp w st C na F
 * 1.8F * ... st C + 32.0 = ...F
 */
public class StopnieCelc {
    public static void main(String[] args) {
        double c;
        double f;
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj wartość temparatury w st C a ja powiem ile to F");
        c = odczyt.nextDouble();
        f = 1.8 * c + 32;
        System.out.println(f + " F");


    }
}
