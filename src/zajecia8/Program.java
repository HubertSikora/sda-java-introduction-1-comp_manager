package zajecia8;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj pierwsza liczbe");
        int liczba = sc.nextInt();
        System.out.println("Podaj 2 liczbę: ");
        int drugaliczba = sc.nextInt();

        // System.out.println("Wynik dzielenia: ");
        try {
            int result = liczba / drugaliczba;
            System.out.println("Wynik: " + result); // w razie bledu wynik sie nie wypisze
            // odrazu przechodzimy to catch. Ale wypisze sie wszystko t oco bylo przed operacją dzielenia w try
        } catch (ArithmeticException Ex) { // w catch wypisujemy co ma zrobic gdyt blad sie pojawi
            System.out.println("Nie mozna dzielic przez 0!");
        }
    }
}
