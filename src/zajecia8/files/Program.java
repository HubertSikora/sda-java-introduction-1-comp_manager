package zajecia8.files;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Klasyczny program wejscia i wyjsca, strumienie, zapis do pliku itp
 */
public class Program {
    public static void main(String[] args) {
        System.out.println("Zawartość pliku: ");
        String line = null;
        BufferedReader reader = null;

            try {
                reader = new BufferedReader(new FileReader("notatka.txt"));
                {
                    try {
                        while ((line = reader.readLine()) != null) //jesli wynik jest rozny od null to odczytano prawidlowo linijkę. Jednoczesnie przypisalismy do zmiennej i sprawdzilismy czy null
                            System.out.println(line);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                //         System.out.println(reader.readLine());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                System.out.println("to się wykona zawsze");
                if (reader != null) {
                    try {
                        reader.close();
                    } catch
                            (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }





